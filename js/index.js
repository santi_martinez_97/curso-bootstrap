//Tooltip
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
	return new bootstrap.Tooltip(tooltipTriggerEl)
})
//Popover
var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
	return new bootstrap.Popover(popoverTriggerEl)
})
		
$(function(){
	$('.carousel').carousel({
		interval: 3000
	});
				
	$('#buyModal').on('show.bs.modal', function(e){
		console.log("El modal se está abriendo");
					
		$('.btn-candy').removeClass('btn-primary');
		$('.btn-candy').addClass('btn-secondary');
		$('.btn-candy').prop('disabled', true);
	})
				
	$('#buyModal').on('shown.bs.modal', function(e){
		console.log("El modal se ha abierto");
	})
				
	$('#buyModal').on('hide.bs.modal', function(e){
		console.log("El modal se está cerrando");
					
		$('.btn-candy').removeClass('btn-secondary');
		$('.btn-candy').addClass('btn-primary');
		$('.btn-candy').prop('disabled', false);
	})
				
	$('#buyModal').on('hidden.bs.modal', function(e){
		console.log("El modal se ha cerrado");
	})
});